package JSON;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JsonEncode {
    public static void main(String[] args){
        JSONObject obj = new JSONObject();
        obj.put("name", "foo");
        obj.put("num", 100);
        obj.put("balance", 1000.21);
        obj.put("is_vip", true);
        System.out.print(obj);

        System.out.println("");

        //dua
        JSONObject obj2 = new JSONObject();
        obj2.put("name", "John");
        obj2.put("age", 30);

        JSONArray jaCar =new JSONArray();
        jaCar.add("Ford");
        jaCar.add("BMW");
        jaCar.add("Inova");
        obj2.put("cars", jaCar);
        System.out.println(obj2);

        //tiga
        JSONObject obj3 = new JSONObject();
        obj3.put("name", "john");
        obj3.put("age", 30);
        JSONObject obj3sub = new JSONObject();
        obj3sub.put("merek","honda");
        obj3sub.put("Tipe","Vario");
        obj3.put("Motor", obj3sub);
        System.out.println(obj3);






    }
}
