package JSON;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

public class JSONDecode {
    public static void main(String[] args) throws IOException, ParseException {
        String json1 = "{\"balance\":1000.21,\"is_vip\":true,\"num\":100,\"name\":\"foo\"}";

        JSONParser parser = new JSONParser();
        Reader reader = new StringReader(json1);

        Object jsonObj = parser.parse(reader);

        JSONObject jsonObject = (JSONObject) jsonObj;

        String name1 = (String) jsonObject.get("name");
        System.out.println("Name = "+name1);

        long num1 = (long) jsonObject.get("num");
        System.out.println("Number = "+num1);

        boolean isVip = (boolean) jsonObject.get("is_vip");
        System.out.println("Is VIP = "+isVip);

        System.out.println(" ");

        //=============================================================

        String json2 = "{\"cars\":[\"Ford\",\"BMW\",\"Inova\"],\"name\":\"John\",\"age\":30}";
        JSONParser parser2 = new JSONParser();
        Reader reader2 = new StringReader(json2);

        Object jsonObj2 = parser2.parse(reader2);

        JSONObject jsonObject2 = (JSONObject) jsonObj2;

        String name2 = (String) jsonObject2.get("name");
        System.out.println("Name = "+name2);

        long age2 = (Long) jsonObject2.get("age");
        System.out.println("Age = "+age2);

        ArrayList<String> allCars = (ArrayList<String>) jsonObject2.get("cars");
        for (String car : allCars){
            System.out.println(car);
        }

        //=======================================================================

        String json3 = "{\"Motor\":{\"Tipe\":\"Vario\",\"merek\":\"honda\"},\"name\":\"john\",\"age\":30}";
        JSONParser parser3 = new JSONParser();
        Reader reader3 = new StringReader(json3);

        Object jsonObj3 = parser3.parse(reader3);

        JSONObject jsonObject3 = (JSONObject) jsonObj3;

        String name3 = (String) jsonObject3.get("name");
        System.out.println("Name = "+name3);

        long age3 = (Long) jsonObject3.get("age");
        System.out.println("Age = "+age3);

        JSONObject joMotor = (JSONObject) jsonObject3.get("motorcycle");
        String merek = (String) joMotor.get("Merek");
        System.out.println("Merek = "+merek);
        String tipe = (String) joMotor.get("Tipe");
        System.out.println("Tipe = "+tipe);
    }

}
