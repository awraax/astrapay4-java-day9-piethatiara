package JSON;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;


public class AssJsonDecode {
    public static void main(String[] args) throws IOException, ParseException {

        String json= "{\"Cars\":" +
                "[{\"Models\":[\"Fiesta\",\"Focus\",\"Mustang\"],\"Name\":\"Ford\"},{\"Models\":[320,\"X3\",\"X5\"],\"Name\":\"BMW\"},{\"Models\":[\"500\",\"Panda\"],\"Name\":\"Fiat\"}]" +
                ",\"Umur\":30,\"Name\":\"John\"}";

        JSONParser parser = new JSONParser();
        Reader reader = new StringReader(json);

        Object jsonObj = parser.parse(reader);

        JSONObject jsonObject = (JSONObject) jsonObj;

        String name = (String) jsonObject.get("Name");
        System.out.println("Name = "+name);

        long age = (Long) jsonObject.get("Umur");
        System.out.println("Age = "+age);

        JSONArray joCar = (JSONArray) jsonObject.get("Cars");
        for (int i = 0; i < joCar.size(); i++){
            JSONObject object = (JSONObject) joCar.get(i);
            String merk = (String) object.get("Name");
            System.out.println("");
            System.out.println("Merk = "+merk);

            System.out.println("Model = ");
            JSONArray carModel = (JSONArray) object.get("Models");
            for (int j = 0; j<carModel.size(); j++){
                System.out.println(carModel.get(j));
            }
        }

    }

}
