package JSON;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class AssJsonEncode {
    public static void main(String[] args) {
        JSONArray model1 = new JSONArray();
        model1.add("Fiesta");
        model1.add("Focus");
        model1.add("Mustang");

        JSONObject subObject1 = new JSONObject();
        subObject1.put("Name", "Ford");
        subObject1.put("Models", model1);

        JSONArray model2 = new JSONArray();
        model2.add(320);
        model2.add("X3");
        model2.add("X5");

        JSONObject subObject2 = new JSONObject();
        subObject2.put("Name", "BMW");
        subObject2.put("Models", model2);

        JSONArray model3 = new JSONArray();
        model3.add(500);
        model3.add("Panda");

        JSONObject subObject3 = new JSONObject();
        subObject3.put("Name", "Fiat");
        subObject3.put("Model", model3);

        JSONArray cars =new JSONArray();
        cars.add(subObject1);
        cars.add(subObject2);
        cars.add(subObject1);

        JSONObject myObj = new JSONObject();
        myObj.put("Name", "John");
        myObj.put("Umur", 30);
        myObj.put("Cars", cars);

        System.out.println(myObj);
    }

}
