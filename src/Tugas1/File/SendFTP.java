package Tugas1.File;

import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.net.ftp.FTP;
public class SendFTP extends Thread{
    public void run(){
        String server = "ftp.dharmakertamandiri.co.id";
        int port = 21;
        String user = "insanastra@dharmakertamandiri.co.id";
        String password = "P@ssw0rd12345";

        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, password);
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            File firstLocalFile = new File("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Day9/src/Tugas1/berkas/FileProses_Pietha.txt");

            String firstRemoteFile = "FileProses_Pietha.txt";
            InputStream inputStream = new FileInputStream(firstLocalFile);

            System.out.println("Start uploading first file");
            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            inputStream.close();
            if (done){
                System.out.println("The first file upload successfully");
            }
        }catch (IOException ex){
            System.out.println("Error: "+ex.getMessage());
            ex.printStackTrace();
        }finally {
            try {
                if (ftpClient.isConnected()){
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            }catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }
}
