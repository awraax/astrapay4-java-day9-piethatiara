package Tugas1.File;

import java.io.FileWriter;
import java.util.ArrayList;

public class FileWrite extends Thread {
    ArrayList<Mahasiswa> arrayMahasiswa = new ArrayList<Mahasiswa>();
    public FileWrite(ArrayList<Mahasiswa> arrayMahasiswa){
        this.arrayMahasiswa = arrayMahasiswa;
    }
    public void run(){
        try {
            FileWriter fileWriter = new FileWriter("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Day9/src/Tugas1/berkas/FileRata.txt");
            fileWriter.write("Nama,NilaiRata-Rata");
            fileWriter.write("\n");
            for (Mahasiswa mahasiswa: arrayMahasiswa){
                fileWriter.write(mahasiswa.getNama() + "," + (mahasiswa.getFisika()+mahasiswa.getKimia()+mahasiswa.getBiologi()));
                fileWriter.write("\n");
            }
            System.out.println("Success");
            fileWriter.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
