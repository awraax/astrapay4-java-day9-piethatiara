package Tugas1.File;

public class Mahasiswa {
    String nama;
    int fisika;
    int kimia;
    int biologi;

    public Mahasiswa(String nama, int fisika, int kimia, int biologi){
        this.nama = nama;
        this.fisika = fisika;
        this.kimia = kimia;
        this.biologi =biologi;
    }

    public String getNama() {
        return nama;
    }

    public int getFisika() {
        return fisika;
    }

    public int getKimia() {
        return kimia;
    }

    public int getBiologi() {
        return biologi;
    }

    public int rata(int fisika, int kimia, int biologi){
        int ratarata = (fisika + kimia + biologi)/3;
        return ratarata;
    }
}
