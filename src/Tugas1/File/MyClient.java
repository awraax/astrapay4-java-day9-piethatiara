package Tugas1.File;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;


public class MyClient {
    public static void main(String[] args) {
        String ip;
        String port;
        try{
            try (InputStream add = new FileInputStream("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Day9/src/Tugas1/berkas/config.properties")) {
                String konteks = "";
                String konteksServer = "";
                Properties prop = new Properties();

                prop.load(add);

                ip = prop.getProperty("ip");
                port = prop.getProperty("port");
                Socket socket = new Socket(ip, Integer.parseInt(port));
                Scanner input = new Scanner(System.in);
                DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
                DataInputStream dis = new DataInputStream(socket.getInputStream());

                ArrayList<Mahasiswa> arrayMahasiswa = new ArrayList<Mahasiswa>();
                do {
                    System.out.println("MENU");
                    System.out.println("1. Connect Socket");
                    System.out.println("2. Create FileProses");
                    System.out.println("3. Tulis ke File, Connect FTP - Kirim FTP");
                    System.out.println("4. Close connection");
                    System.out.println("99. Exit");
                    System.out.print("Input pilihan : ");
                    konteks = input.next();
                    if (konteks.equalsIgnoreCase("1")) {
                        dout.writeUTF(konteks);
                        dout.flush();
                        konteksServer = (String) dis.readUTF();
                        System.out.println(konteksServer);

                    } else if (konteks.equalsIgnoreCase("2")) {
                        try {
                            FileWriter fileWriter = new FileWriter("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Day9/src/Tugas1/berkas/FileProses_Pietha");
                            String strParsed [] =konteksServer.split("\n");

                            for (int i = 0;i<strParsed.length;i++){
                                String strParsed2[] = strParsed[i].trim().split(",");

                                String nama = strParsed2[0];
                                int fisika = Integer.parseInt(strParsed2[1]);
                                int kimia = Integer.parseInt(strParsed2[2]);
                                int biologi = Integer.parseInt(strParsed2[3]);

                                Mahasiswa mahasiswa = new Mahasiswa(nama, fisika, kimia, biologi);
                                arrayMahasiswa.add(mahasiswa);
                            }
                            for (Mahasiswa mahasiswa: arrayMahasiswa){
                                fileWriter.write("Nama = "+mahasiswa.getNama());
                                fileWriter.write("\n");
                                fileWriter.write("Fisika = "+mahasiswa.getFisika());
                                fileWriter.write("\n");
                                fileWriter.write("Kimia = "+mahasiswa.getKimia());
                                fileWriter.write("\n");
                                fileWriter.write("Biologi = "+mahasiswa.getBiologi());
                            }
                            System.out.println("Success");
                            fileWriter.close();
                        }catch (Exception e){
                            System.out.println(e);
                        }
                        dout.writeUTF(konteks);
                        dout.flush();

                    } else if (konteks.equalsIgnoreCase("3")) {
                        FileWrite threadSatu = new FileWrite(arrayMahasiswa);
                        SendFTP threadDua = new SendFTP();
                        threadSatu.start();
                        threadDua.start();
                        dout.writeUTF(konteks);
                        dout.flush();

                    } else if (konteks.equalsIgnoreCase("4")) {
                        dout.close();
                        socket.close();

                    } else if (konteks.equalsIgnoreCase("99")) {
                            break;
                    } else System.out.println("Pilihan menu tidak ada");
                }while (true);

            }catch (Exception e){
                System.out.println(e);
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
