package Tugas1.File;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

public class MyServer {
    public static void main(String[] args) {
        String port = "";
        String data = "";

        try{
            try(InputStream input = new FileInputStream("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Day9/src/Tugas1/berkas/config.properties")){

                Properties prop = new Properties();
                prop.load(input);
                port = prop.getProperty("port");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            FileReader fileReader = new FileReader("/Users/ada-nb182/Documents/PTDP/Hard Skills PPT/Pemrograman/Java/Day9/src/Tugas1/berkas/FilePietha.txt");
            int i =0;

            while ((i = fileReader.read()) != -1){
                data = data + (char) i;
            }
            fileReader.close();

            ServerSocket serverSocket = new ServerSocket(Integer.parseInt(port));
            Socket socket = serverSocket.accept();
            String konteks = "";
            while (!konteks.equalsIgnoreCase("99")){
                DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
                DataInputStream dis = new DataInputStream(socket.getInputStream());
                konteks = (String) dis.readUTF();

                if (konteks.equalsIgnoreCase("1")){
                    System.out.println("Client = " +konteks);
                    dout.writeUTF(data);
                    dout.flush();
                } else if (konteks.equalsIgnoreCase("2")) {
                    System.out.println("Client = " +konteks);
                    dout.writeUTF(data);
                    dout.flush();
                } else if (konteks.equalsIgnoreCase("3")){
                    System.out.println("Client = " +konteks);
                    dout.writeUTF(data);
                    dout.flush();
                } else if (konteks.equalsIgnoreCase("4")) {
                    serverSocket.close();
                    break;
                }else {
                    System.out.println("Client exit");
                    break;
                }
            }
            serverSocket.close();
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
