package Tugas1.ftp;

import org.apache.commons.net.ftp.FTPClient;

import java.io.*;
import org.apache.commons.net.ftp.FTP;

public class FTPDownload {
    public static void main(String[] args) {
        String server = "ftp.dharmakertamandiri.co.id";
        int port = 21;
        String user = "insanastra@dharmakertamandiri.co.id";
        String password = "P@ssw0rd12345";

        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, password);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            String remoteFile1 = "/testpietha.txt";
            File downloadFile1 = new File("/Users/ada-nb184/Downloads/testpietha.txt");
            OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
            boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
            outputStream1.close();

            if (success) {
                System.out.println("File has been downloaded successfully.");
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
